# TestingBootcamp
New Bootcamp using Atlassian products

##Local Development

The local development environment is managed by [Vagrant](https://www.vagrantup.com/), therefore you will need to have it installed. Also, you need to download [Ansible](http://www.ansible.com/), and [VirtualBox](https://www.virtualbox.org/) manually.

Once you have these dependencies and the code cloned to your machine, you can provision your development environment with:

    vagrant up

Once the development environment is provisioned, all environment management is performed through Vagrant. See the [documentation](https://docs.vagrantup.com/v2/cli/index.html) for further details. Another helpful command to remember is:

    vagrant provision

This will update your machine to the latest setup.


###Build and Run

To access your development environment, run:

    vagrant ssh

From the folder where the app sits run:

    bundle exec 'shotgun' -p 8000 -o "\*"

To interact with the application, navigate to `http://localhost:8000`.
